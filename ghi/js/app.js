function createCard(name, description, pictureUrl, location, sday, smonth, syear, eday, emonth, eyear,) {
    return `
    <div class="col-sm-6 col-md-4 mb-1">
        <div class="shadow-lg p-3 mb-5 bg-body rounded">
            <div class="card">
                <img src="${pictureUrl}" class="card-img-top">
                <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                <p class="card-text">${description}</p>
            </div>
                <div class="card-footer">
                <small class="text-muted">${smonth + 1}/${sday}/${syear}-${emonth + 1 }/${eday}/${eyear}</small>
            </div>
        </div>
    </div>
    `
  }
  function errors(){
    return `
    <div class="alert alert-primary d-flex align-items-center" role="alert">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-exclamation-triangle-fill flex-shrink-0 me-2" viewBox="0 0 16 16" role="img" aria-label="Warning:">
            <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
        </svg>
        <div>
            Server error
        </div>
    </div>`
  }
window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    try {
        const response = await fetch(url)
        if(!response.ok) {
            throw new Error('Response not ok')
        } else {
        const data = await response.json()

        for(let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`
            const detailResponse = await fetch(detailUrl)
            if(detailResponse.ok) {
                const details = await detailResponse.json()
                const title = details.conference.name
                const description = details.conference.description
                const pictureUrl = details.conference.location.picture_url
                const startDate = new Date(details.conference.starts)
                let sDay = startDate.getDate()
                let sMonth = startDate.getMonth()
                let sYear = startDate.getFullYear()
                const endDate = new Date(details.conference.ends)
                let eDay = endDate.getDate()
                let eMonth = endDate.getMonth()
                let eYear = endDate.getFullYear()
                const location = details.conference.location.name
                const html = createCard(title, description, pictureUrl, location, sDay, sMonth, sYear, eDay, eMonth, eYear)
                const column = document.querySelector('.row')
                column.innerHTML += html
            }
        }
    }
    } catch(e) {
        const alert = errors()
        document.querySelector("alert alert-primary d-flex align-items-center")

        console.error("Failed to fetch")
    }

});

window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/states/';
    try {
        const response = await fetch(url)
        if(!response.ok) {
            throw new Error('Response not ok')
        } else {
        const data = await response.json()
        console.log(data)
            // Get the select tag element by its id 'state'
        const selectTag = document.getElementById('state')
        for(let state of data.states) {
            let option = document.createElement('option')// Create an 'option' element
            option.value = state["abbreviation"] // Set the '.value' property of the option element to the
                                                // state's abbreviation
            option.innerHTML = state["name"]  // Set the '.innerHTML' property of the option element to
                                                // the state's name
            selectTag.appendChild(option) // Append the option element as a child of the select tag
        }
        const formTag = document.getElementById('create-location-form')
        formTag.addEventListener('submit', async event => {
            event.preventDefault()
            console.log('need to submit the form data')
            const formData = new FormData(formTag)
            const json = JSON.stringify(Object.fromEntries(formData))
            const locationUrl = 'http://localhost:8000/api/locations/'
            const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                }
            }
            const response = await fetch(locationUrl, fetchConfig)
            if(response.ok) {
                formTag.reset()
                const newLocation = await response.json()
            }
        })
        }
    }
    catch(e) {
        const alert = errors()
        document.querySelector("alert alert-primary d-flex align-items-center")

        console.error("Failed to fetch")
    }

});
